#!/bin/bash

FILE=.configured

help()
{
    echo "Allows to build the project"
    echo
    echo "Syntax: [-a|f|h]"
    echo "Options: "
    echo "a     Build all"
    echo "f     Configure project. To be called at least once before building"
    echo "h     Prints this help"
}

build_first()
{
    if test -f "$FILE"; then
        echo "Project already configured"
    else
        touch .configured
        
        echo "===Updating git==="
        sudo apt update
        sudo apt install git
        sudo apt install software-properties-common curl
        sudo add-apt-repository ppa:git-core/ppa
        curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
        sudo apt install git-lfs
        git lfs install

        echo "===Installing toolchain==="
        sudo mkdir -p /opt/gcc-Toolchain-2019.12/
        sudo git clone https://github.com/WAGO/gcc-toolchain-2019.12-precompiled.git /opt/gcc-Toolchain-2019.12/

        echo "===Installing tools==="
        sudo apt install libncurses5-dev gawk flex bison texinfo python-dev g++ dialog libc6-dev lzop autoconf libtool xmlstarlet xsltproc doxygen autopoint python3-setuptools
        
        echo "===Building PTXDIST==="
        cd ptxdist
        ./configure
        make
        sudo make install
        cd ..

        echo "===Configuring project==="
        cd pfc-firmware-sdk-G2
        ptxdist select configs/wago-pfcXXX/ptxconfig_pfc_g2
        ptxdist platform configs/wago-pfcXXX/platformconfig
        ptxdist toolchain /opt/gcc-Toolchain-2019.12/arm-linux-gnueabihf/bin/
        cd ..
        echo "===Project configured==="
    fi
}

build_all()
{
    if test -f "$FILE"; then
        cd pfc-firmware-sdk-G2
        ptxdist go -q
        ptxdist images
        cd ..
    else
        echo "Project not configured. Use -f to perform first time configuration"
    fi
}

if [ $# -eq 0 ];
then
    echo "Error: missing argument" 
    help
    exit 0
else

while getopts ":afh" option; do
    case $option in
        a)
            build_all
            exit;;
        f)
            build_first
            exit;;
        h)
            help
            exit;;
        \?)
            echo "Error: invalid option"
            help
            exit;;
    esac
done
fi
