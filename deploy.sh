#!/bin/bash

help()
{
    echo "Allows to deploy to a device"
    echo
    echo "Syntax: [-d|h|n]"
    echo "Options: "
    echo "d     Device to write (sdx)"
    echo "h     Prints this help"
    echo "n     Deploy nfs rootfs"
}

deploy()
{
    dev="/dev/$1"
    echo "Deploying to $dev"

    SDCARD="$(lsblk -o KNAME,TYPE,SIZE,MODEL | grep 'STORAGE_DEVICE')"

    case $SDCARD in
        *$1*) 
            echo "SD card found"

            sudo umount $dev"1"
            sudo umount $dev"2"

            sudo dd if=/dev/zero of=$dev bs=1MB count=300 conv=notrunc status=progress oflag=sync
            sudo dd if=pfc-firmware-sdk-G2/platform-wago-pfcXXX/images/sd.hdimg of=$dev bs=16KB status=progress oflag=sync

            sudo fatlabel $dev"1" BOOT
            sudo e2label $dev"2" rootfs

            sudo mkdir -p /media/tmp
            sudo mount $dev"2" /media/tmp
            sudo cp rootfs_overlay/etc/specific/netconfd.json /media/tmp/etc/specific
            sudo umount $dev"2"
            sudo rm -rf /media/tmp

            sudo eject $dev
            exit;;
        *)
            echo "$dev is not an SD card"
            exit;;
    esac
}

deploy_nfs()
{
    # echo "Deploying rootfs on NFS"
    # sudo rm -rf nfs_share/*
    # sudo tar -xf output/images/rootfs.tar -C nfs_share/

    echo "Not implemented"
}

if [ $# -eq 0 ];
then
    echo "Error: missing argument" 
    help
    exit 0
else

while getopts ":dhn" option; do
    case $option in
        d)
            if [ "$#" -ne 2 ]; then
                echo "Missing device sd[x]"
            else
                deploy "$2"
            fi
            exit;;
        h)
            help
            exit;;
        n)
            deploy_nfs
            exit;;
        \?)
            echo "Error: invalid option"
            help
            exit;;
    esac
done
fi
