#include <pistache/endpoint.h>
#include <iostream>
#include <csignal>

#include "server.h"

using namespace Pistache;

Address addr ("*:9080");
Http::Endpoint endpoint(addr);

RestServer::Server server;
int end = 0;

struct HelloHandler : public Http::Handler
{
	HTTP_PROTOTYPE(HelloHandler)
	void onRequest(const Http::Request &, Http::ResponseWriter writer) override
	{
		std::cout << __func__ << std::endl;
		writer.send(Http::Code::Ok, "Hello, World!");
	}
};

void sig_handler(int sig)
{
	switch (sig)
	{
	case SIGINT:
		std::cout << __func__ << std::endl;
		end = 1;

		break;
	
	default:
		break;
	}
}

#if 0
int main()
{
	signal(SIGINT, sig_handler);
	std::cout << "Launching rest server" << std::endl;

	endpoint.init();
	endpoint.setHandler(Http::make_handler<HelloHandler>());
	endpoint.serve();
}
#else 

int main()
{
	signal(SIGINT, sig_handler);
	std::cout << "Launching rest server" << std::endl;
	server.start();

	while (!end)
	{
		usleep(1000);
	}

	server.stop();

	return 0;
}

#endif