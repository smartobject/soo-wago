cmake_minimum_required(VERSION 3.16.0)

file(GLOB HOST_DIR ${CMAKE_CURRENT_LIST_DIR})

# message(${HOST_DIR})

set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR armv7l)

set(CMAKE_SYSROOT "/opt/gcc-Toolchain-2019.12/arm-linux-gnueabihf-sysroot")
# set(CMAKE_SYSTEM_INCLUDE_PATH ../pfc-firmware-sdk-G2/platform-wago-pfcXXX/sysroot-target/usr/include/)
# set(CMAKE_SYSROOT "~/Project/soo_mp/soo-wago/pfc-firmware-sdk-G2/platform-wago-pfcXXX/sysroot-target")
# set(CMAKE_SYSROOT_COMPILE "../pfc-firmware-sdk-G2/platform-wago-pfcXXX/sysroot-cross")
# set(CMAKE_SYSROOT_LINK "../pfc-firmware-sdk-G2/platform-wago-pfcXXX/sysroot-cross")
# set(CMAKE_SYSROOT_COMPILE "/opt/gcc-Toolchain-2019.12/arm-linux-gnueabihf-sysroot")
# set(CMAKE_SYSROOT_LINK "/opt/gcc-Toolchain-2019.12/arm-linux-gnueabihf-sysroot")

# set(tools "/opt/gcc-Toolchain-2019.12/arm-linux-gnueabihf/bin/arm-linux-gnueabihf-")

set(tools ${HOST_DIR}/../pfc-firmware-sdk-G2/platform-wago-pfcXXX/sysroot-host/lib/wrapper/arm-linux-gnueabihf-)
set(CMAKE_C_COMPILER "${tools}gcc")
set(CMAKE_CXX_COMPILER "${tools}g++")

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)