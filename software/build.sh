#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
PISTACHE_PATH=$SCRIPTPATH/rest/pistache
BUILD_DALISERVER="-DDALISERVER_BUILD=ON"
CONFIGURED=.configured
TOOLCHAIN_FILE=$SCRIPTPATH/ptxdist_toolchain.cmake


clean()
{
    rm -rf $SCRIPTPATH/build
    rm -rf $SCRIPTPATH/output
    # rm -rf $PISTACHE_PATH/build
    rm $SCRIPTPATH/$CONFIGURED
}

configure()
{
    build_lib
    touch $CONFIGURED
}

build_user()
{
    cmake -DCMAKE_TOOLCHAIN_FILE=$TOOLCHAIN_FILE -S . -B build
    cmake --build build
}

build_lib()
{
    cd $PISTACHE_PATH
    cmake -DCMAKE_TOOLCHAIN_FILE=$TOOLCHAIN_FILE -S . -B build
    cmake --build build
    cd $SCRIPTPATH
}

cpy_to_output()
{
    mkdir -p $SCRIPTPATH/output/usr/lib 

    cp $SCRIPTPATH/build/daliserver/daliserver $SCRIPTPATH/output/usr
    cp -r $PISTACHE_PATH/build/src/libpistache.s* $SCRIPTPATH/output/usr/lib
    cp $SCRIPTPATH/build/testserver/testserver $SCRIPTPATH/output/usr
    cp $SCRIPTPATH/build/rest/server/librestserver.so $SCRIPTPATH/output/usr/lib
    cp $SCRIPTPATH/testserver/config.json $SCRIPTPATH/output/usr
}

help()
{
    echo "Allows to compile a package"
    echo
    echo "Syntax: [-a|c|h|l|u]"
    echo "Options: "
    echo "c     Clean projects"
    echo "h     Prints this help"
    echo "l     Build libraries"
    echo "u     Build user applications"
}

if [ $# -eq 0 ];
then
    echo "Error: missing argument" 
    help
    exit 0
else 

if ! test -f "$CONFIGURED"; then
    configure
fi

while getopts ":achlu" option; do
    case $option in
        c)
            clean
            exit;;

        h)  
            help
            exit;;
        l)
            build_lib
            cpy_to_output
            exit;;
        u)
            build_user
            cpy_to_output
            exit;;
        \?)
            echo "Error: invalid option"
            help
            exit;;
    esac
done
fi

