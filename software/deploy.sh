#!/bin/bash

IP="192.168.1.10"
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
PKG_DIR="../../pfc-firmware-sdk-G2/platform-wago-pfcXXX/packages"
PKG_NAME="daliserver"
VERSION="1.0"
EXT="armhf.ipk"
DEST_FOLDER="/usr"

help()
{
    echo "Allows to deploy to a device"
    echo
    echo "Syntax: [-d|h|n]"
    echo "Options: "
    echo "d     Device to write (sdx)"
    echo "h     Prints this help"
    echo "s     Generate and deploy app through ssh"
}

deploy()
{
    dev="/dev/$1"
    echo "Deploying to $dev"

    SDCARD="$(lsblk -o KNAME,TYPE,SIZE,MODEL | grep 'STORAGE_DEVICE')"

    case $SDCARD in
        *$1*) 
            echo "SD card found"

            sudo umount $dev"1"
            sudo umount $dev"2"

            sudo dd if=/dev/zero of=$dev bs=1MB count=200 conv=notrunc status=progress oflag=sync
            sudo dd if=pfc-firmware-sdk-G2/platform-wago-pfcXXX/images/sd.hdimg of=$dev bs=16KB status=progress oflag=sync

            sudo fatlabel $dev"1" BOOT
            sudo e2label $dev"2" rootfs

            sudo mkdir -p /media/tmp
            sudo mount $dev"2" /media/tmp
            sudo cp rootfs_overlay/etc/specific/netconfd.json /media/tmp/etc/specific
            sudo umount $dev"2"
            sudo rm -rf /media/tmp

            sudo eject $dev
            exit;;
        *)
            echo "$dev is not an SD card"
            exit;;
    esac
}

deploy_ssh()
{
    # scp $PKG_DIR/${PKG_NAME}_${VERSION}_$EXT root@$IP:$DEST_FOLDER
    scp -rp $SCRIPTPATH/output/usr/* root@$IP:/usr
    # rsync -ru $SCRIPTPATH/output/usr/* root@$IP:/usr
}

if [ $# -eq 0 ];
then
    echo "Error: missing argument" 
    help
    exit 0
else

while getopts ":dhs" option; do
    case $option in
        d)
            # if [ "$#" -ne 2 ]; then
            #     echo "Missing device sd[x]"
            # else
            #     deploy "$2"
            # fi
            echo "Not implemented"
            exit;;
        h)
            help
            exit;;
        s)
            # ./build.sh -g
            deploy_ssh
            exit;;
        \?)
            echo "Error: invalid option"
            help
            exit;;
    esac
done
fi
