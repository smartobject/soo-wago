#pragma once

#include "route.h"
#include "response.h"

#include "pistache/endpoint.h"

namespace RestServer{

    struct json_field{
        std::string key;
        std::string value;
    };

    /**
     * @brief Server calls interface to use the rest server via the pistache.io api
     * 
     */
    class Server
    {
        public:
            /**
             * @brief syntactic sugar for Pistache::Rest::Request&
             * 
             */
            typedef const Pistache::Rest::Request& request_t;

            /**
             * @brief Construct a new Server object
             * 
             * @param[in] host optional ip address of the server : std::string 
             * @param[in] port optional port number of the server : std::string 
             */
            Server(std::string host = "", std::string port = "");

            /**
             * @brief Adds a new route to the server
             * 
             * @tparam A function pointer
             * @tparam B object pointer
             * @param[in] func_ptr pointer to the callback function for the route : A
             * @param[in] obj_ptr pointer to the object having the callback function : B
             * @param[in] req type of request (GET, POST, ...) : request_t
             * @param[in] route route address on the server : std::string
             */
            template<typename A, typename B>
            void addRoute(A func_ptr, B obj_ptr = nullptr, Route::request_t req = Route::NONE, std::string route = "")
            {
                Route *_route = new Route(req, route);
                routes.push_back(_route);
                routes.back()->Bind(std::bind(func_ptr, obj_ptr, std::placeholders::_1));
                routes.back()->addRoute(&this->router);
            }

            /**
             * @brief Start the server
             * 
             */
            void start();

            void stop();

        private:
            Pistache::Rest::Router router;
            Pistache::Address addr;
            std::list<Route*> routes;

            std::string baseHost = "127.0.0.1";
            std::string basePort = "8080";
            std::string name;
            std::string type;

            std::shared_ptr<Pistache::Http::Endpoint> endpoint;

            std::list<json_field> other_infos;

            /**
             * @brief Reads the server_config.json file
             * 
             */
            void read_config();

            /**
             * @brief Base route callback that return the information of the config file
             * 
             * @param[in] data the body of the request 
             * @return Response 
             */
            Response Info(const Pistache::Rest::Request& req);

            /**
             * @brief Base route callback to check if server is online
             * 
             * @param[in] data the body of the request 
             * @return Response 
             */
            Response Index(const Pistache::Rest::Request& req);

            /**
             * @brief Base route callback for route not found 404
             * 
             * @param[in] data the body of the request 
             * @return Response 
             */
            Response NotFound(const Pistache::Rest::Request& req);
    };
    
}//RestServer