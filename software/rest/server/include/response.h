#pragma once

#include <iostream>
#include "pistache/router.h"

namespace RestServer
{
    /**
     * @brief Response class the route callback function must return an object of this class
     * 
     */
    class Response
    {
        public:
            /**
             * @brief syntactic sugar for Http possible return codes
             * 
             */
            typedef Pistache::Http::Code HttpStatus;
            
        private:
            HttpStatus _code;
            std::string _data = nullptr;
        
        public :
            /**
             * @brief Construct a new Response object
             * 
             * @param[in] code the Http status if the response : HttpStatus
             * @param[in] data the data to send : std::string&
             */
            Response(HttpStatus code, std::string& data);

            /**
             * @brief Construct a new Response object
             * 
             * @param code the Http status if the response : HttpStatus
             * @param data the data to send : const char*
             */
            Response(HttpStatus code, const char* data);

            /**
             * @brief Get the code value
             * 
             * @return HttpStatus 
             */
            HttpStatus get_code() { return _code; }

            /**
             * @brief Get the data string
             * 
             * @return std::string 
             */
            std::string get_str();
    };
}