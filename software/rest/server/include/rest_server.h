#pragma once

namespace RestServer
{
    /**
     * @brief Singlenton class RestServer
     * 
     */
    class RestServer
    {
        public:
            /**
             * @brief Get the Instance object
             * 
             * @return RestServer& : Return an instance of RestServer
             */
            static RestServer& getInstance()
            {
                static RestServer instance;
                return instance;
            }
        
        private:
            RestServer() {}

            
        
        public:
            RestServer(RestServer const&) = delete;
            void operator=(RestServer const&) = delete;

            /**
             * @brief Add all the defined routes (class Routes) and start the server
             * 
             */
            void start();

            /**
             * @brief Checks wheter the server is running
             * 
             * @return true 
             * @return false 
             */
            bool isStarted();
    };
}
