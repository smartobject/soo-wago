#pragma once

#include <functional>
#include <string>

#include "response.h"
#include "pistache/router.h"

namespace RestServer
{
    /**
     * @brief Class that declares a route for the server
     * 
     */
    class Route{
        friend class Server;
        public:
            /**
             * @brief Enum describing the type of request
             * 
             */
            enum request_t{GET, POST, PUT, DELETE, NOT_FOUND, NONE};

            /**
             * @brief Typedef of the callback function implemented by the route with a body
             * 
             */
            typedef std::function<Response(const Pistache::Rest::Request& req)> callback_t;  

        private:
            std::string _route;
            request_t _req;
            callback_t callback = nullptr;
            
            /**
             * @brief Construct a new Route object
             * 
             * @param[in] req request type : request_t
             * @param[in] route string containing the route in the server : std::string
             */
            Route(request_t req = NONE, std::string route = "") : _req(req), _route(route) {}

            /**
             * @brief Binds the callback function of the route with the given function
             * 
             * @param[in] cb callback function : callback_t
             */
            void Bind(callback_t cb) { callback = cb; }

            /**
             * @brief Add the route to the pistache route handler
             * 
             * @param[in] router router to add the route to : void* 
             */
            void addRoute(void* router);

            /**
             * @brief Called by the pistache route is asked for. It calls the calls the user callback function and does some stuff
             * 
             * @param[in] req Request data given by the server 
             * @param[in] res Resonse writer to use to give a response
             */
            void _callback(const Pistache::Rest::Request& req, Pistache::Http::ResponseWriter res);     
    };
}