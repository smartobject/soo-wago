#include "rest_server.h"
#include "server.h"

namespace RestServer
{
    static bool isRunning = false;

    /**
     * @brief Class containing the callback for all the server routes
     * 
     */
    class Routes
    {
        public:
            /**
             * @brief Example method to add to this class 
             * @param data : containts the request body if there is one
             * @return Response : Object of the Response class with a string formatted as json or not              
             */
            Response Example(const Pistache::Rest::Request& req)
            {
                //Do stuff

                std::cout << req.body() << std::endl;

                return Response(Response::HttpStatus::Ok, "Example");
            }
    };

    bool RestServer::isStarted()
    {
        return isRunning;
    }

    void RestServer::start()
    {
        if(!isRunning)
        {
            Server server;
            Routes routes;
            
            isRunning = true;

            server.addRoute(&Routes::Example, routes, Route::PUT, std::string("/example"));

            server.start();
        }
    }
}