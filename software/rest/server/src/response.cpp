#include "response.h"

#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"
#include "rapidjson/document.h"

namespace RestServer
{
    Response::Response(HttpStatus code, std::string& data) : _code(code), _data(data) {}

    Response::Response(HttpStatus code, const char* data) : _code(code), _data(std::string(data)) {} 

    std::string Response::get_str()
    {
        rapidjson::StringBuffer s;
        rapidjson::Writer<rapidjson::StringBuffer> writer(s);
        rapidjson::Document d;

        writer.StartObject();
        writer.Key("Result");
        
        if(_code == HttpStatus::Ok) writer.String("Ok");
        else writer.String("Failed");

        writer.Key("Data");

        d.Parse(_data.c_str());

        if(d.HasParseError())
        {
            writer.String(_data.c_str());
        }
        else
        {
            d.Accept(writer);
        } 

        writer.EndObject();

        return std::string(s.GetString());
    }
}