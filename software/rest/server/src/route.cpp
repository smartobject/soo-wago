#include <ctime>
#include <iomanip>
#include <functional>
#include <typeinfo>

#include "route.h"

namespace RestServer
{
    void printResponse(const Pistache::Rest::Request& req, Pistache::Http::Code code)
    {
        Pistache::Address sender_addr = req.address();
        std::time_t t = std::time(nullptr);
        std::cout   << "[" 
                    << std::put_time(std::localtime(&t), "%c %Z")
                    << "] "
                    << "Received request " 
                    << req.method() 
                    << " from " 
                    << sender_addr.host() 
                    << " --> "
                    << req.version()
                    << " : "
                    << static_cast<int>(code)
                    << " "
                    << code
                    << std::endl;
    }

    void Route::addRoute(void* _router)
    {
        Pistache::Rest::Router *router = (Pistache::Rest::Router*) _router;

        switch (_req)
        {
            case GET:
                Pistache::Rest::Routes::Get(*router, _route, Pistache::Rest::Routes::bind(&Route::_callback, this));
                break;

            case POST:
                Pistache::Rest::Routes::Post(*router, _route, Pistache::Rest::Routes::bind(&Route::_callback, this));
                break;
            
            case PUT:
                Pistache::Rest::Routes::Put(*router, _route, Pistache::Rest::Routes::bind(&Route::_callback, this));
                break;

            case DELETE:
                Pistache::Rest::Routes::Delete(*router, _route, Pistache::Rest::Routes::bind(&Route::_callback, this));
                break;
            
            case NOT_FOUND:
                Pistache::Rest::Routes::NotFound(*router, Pistache::Rest::Routes::bind(&Route::_callback, this));
            
            default:
                break;
        }
    }

    void Route::_callback(const Pistache::Rest::Request& req, Pistache::Http::ResponseWriter res)
    {
        Response *resp;
        if(this->callback)
        {
            resp = new Response(callback(req));
        }
        res.send(resp->get_code(), resp->get_str());
        res.headers().add<Pistache::Http::Header::ContentType>
            (Pistache::Http::Mime::MediaType(Pistache::Http::Mime::Type::Application, Pistache::Http::Mime::Subtype::Json));
        
        printResponse(req, resp->get_code());

        delete(resp);
    }

}