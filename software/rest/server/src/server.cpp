#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdio>
#include <exception>
#include <algorithm>
#include <list>

#include "server.h"

#include "pistache/mime.h"
#include "pistache/router.h"

#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"
#include "rapidjson/reader.h"

namespace RestServer{

    Response Server::Index(const Pistache::Rest::Request& req)
    {
        return Response(Response::HttpStatus::Ok, "Index");
    }

    Response Server::NotFound(const Pistache::Rest::Request& req)
    {
        return Response(Response::HttpStatus::Not_Found, "Route not found");
    }

    Response Server::Info(const Pistache::Rest::Request& req)
    {
        rapidjson::StringBuffer s;
        rapidjson::Writer<rapidjson::StringBuffer> writer(s);
        std::list<Route*>::iterator it;
        std::list<json_field>::iterator str_it;

        writer.StartObject();

        for(str_it = other_infos.begin(); str_it != other_infos.end(); str_it++)
        {
            writer.Key(str_it->key.c_str());
            writer.String(str_it->value.c_str());
        }

        writer.Key("Routes");
        writer.StartArray();

        for(it = routes.begin(); it != routes.end(); it++)
        {
            writer.String((*it)->_route.c_str());
        }

        writer.EndArray();
        writer.EndObject();

        return Response(Pistache::Http::Code::Ok, s.GetString());
    }

    Server::Server(std::string host, std::string port)
    {
        read_config();

        if(host.empty()) host = this->baseHost;
        if(port.empty()) port = this->basePort;

        addr = Pistache::Address(host, Pistache::Port(port));

        addRoute(&Server::Info, this, Route::GET, std::string("/info"));
        addRoute(&Server::NotFound, this, Route::NOT_FOUND);
        addRoute(&Server::Index, this, Route::GET, "/");
    }

    std::string search_in_json_obj(const char* key, rapidjson::GenericObject<false, rapidjson::Value> obj)
    {
        if(obj.HasMember(key))
        {
            if(obj[key].IsString())
            {   
                return std::string(obj[key].GetString());
            }
        }
        return nullptr;
    }

    void Server::read_config()
    {
        FILE *fp = fopen("server_config.json", "r");
        char buf[10000];
        rapidjson::Document d;
        rapidjson::FileReadStream json(fp, buf, sizeof(buf));   

        if(fp == NULL)
        {
            throw std::invalid_argument("Could not find config file");
        }

        d.ParseStream(json);

        fclose(fp);

        if(d["Description"].IsObject())
        {
            auto obj = d["Description"].GetObject();

            auto begin = obj.MemberBegin();

            while (begin != obj.MemberEnd())
            {
                json_field j = {std::string(begin->name.GetString()), std::string(begin->value.GetString())};
                other_infos.push_back(j);
                begin++;
            }
            this->baseHost = search_in_json_obj("Default_host", obj);
            this->basePort = search_in_json_obj("Default_port", obj);
        } 
    }

    void Server::start()
    {
        auto opts = Pistache::Http::Endpoint::options()
            .threads(2)
            .flags(Pistache::Tcp::Options::ReuseAddr);
    
        endpoint = std::make_shared<Pistache::Http::Endpoint>(addr);
        endpoint->init(opts);
        endpoint->setHandler(router.handler());
        std::cout << "Starting server at " << addr.host() << ":" << addr.port() << std::endl;
        endpoint->serveThreaded();
    }

    void Server::stop()
    {
        endpoint->shutdown();
    }
} //namespace