/*
 * Copyright (C) 2021-2022 Mattia Gallacchi <mattia.gallacchi@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#pragma once

 #include "common.h"

#define DEVICE_TOGGLE               0x1

namespace KBUS {
    namespace DALI {

        /**
         * @brief Current status
         * 
         */
        enum STATUS {OFF = 0, ON};

        /**
         * @brief Dali device type
         * 
         */
        enum TYPE {LED = 0, SWITCH};

        static const char type_str[][MAX_CHAR_STR] = {
            "Led", 
            "Switch"
        };

        /**
         * @brief Device status struct
         * 
         * @param status ON/OFF 1/0
         * @param err Error true/false 1/0
         */
        struct dev_status_t
        {
            int status;
            int err;
        };

        /**
         * @brief Position of the devices values inside the process data
         * 
         */
        struct data_pos_t 
        {
            int byte;
            int bit;
        };

        /**
         * @brief Dali device base class
         * 
         */
        class GenericDevice {

            public:
                /**
                 * @brief Destroy the Generic Device object
                 * 
                 */
                virtual ~GenericDevice() = 0;

                /**
                 * @brief Update device status struct
                 * 
                 * @param s status struct read from process data. Modify if different to update 
                 *          the status
                 */
                virtual void update_status(dev_status_t& s) = 0;

                /**
                 * @brief Get device type
                 * 
                 * @return device type
                 */
                TYPE get_type();

                /**
                 * @brief Get the data pos struct
                 * 
                 * @return data_pos_t 
                 */
                data_pos_t get_data_pos();

                int get_id();

                dev_status_t get_status();
                
            protected:
                // Device id. Starts at 0
                int m_id;

                // Device type
                TYPE m_type;

                // Device current status
                dev_status_t m_status;

                // Device data posistion in process images
                data_pos_t m_data_pos;

                // Used to wait the status change 
                bool changing;

                /**
                 * @brief Construct a new Generic Device object
                 * 
                 * @param type Device type
                 * @param id Device id
                 * @param byte_num Process data byte containing data for this device
                 * @param bit_pos Position of the first bit LSB in the byte_num
                 */
                GenericDevice(TYPE type, int id, int byte_num, int bit_pos);

        };

        /**
         * @brief Dali led device class
         * 
         */
        class Led : public GenericDevice
        {
            public:
                /**
                 * @brief Construct a new Led object
                 * 
                 * @param id Device id
                 * @param byte_num Process data byte containing data for this device
                 * @param bit_pos Position of the first bit LSB in the byte_num
                 */
                Led(int id, int byte_num, int bit_pos);

                /**
                 * @brief Destroy the Led object
                 * 
                 */
                ~Led();

                /**
                 * @brief Update device status struct
                 * 
                 * @param s status struct read from process data. Modify if different to update 
                 */   
                void update_status(dev_status_t& s);

                /**
                 * @brief Invert the current status
                 * 
                 */
                void toggle();

                /**
                 * @brief Set the led status 
                 * 
                 * @param status 0 : off, 1 : on
                 */
                void set_status(int status);
        };


    }// end DALI
}// end KBUS