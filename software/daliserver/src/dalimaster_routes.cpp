/*
 * Copyright (C) 2021-2022 Mattia Gallacchi <mattia.gallacchi@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "dalimaster.h"

#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"
#include "rapidjson/reader.h"

using namespace RestServer;

namespace KBUS::DALI {

    int extract_id(std::string query, std::string& id)
    {
        size_t id_pos, id_end;

        if ((id_pos = query.find("id")) == std::string::npos) {
            return -1;
        }

        query = query.substr(id_pos + 3);

        id_end = query.find('&');

        if (id_end != std::string::npos) {
            query.erase(id_end, query.length());
        }

        id = query;
        return 0;
    }

    int split_str(std::string str, std::vector<std::string>& tokens, char delimiter)
    {
        int tok = 0;
        size_t pos;

        while((pos = str.find(delimiter)) != std::string::npos)
        {
            tok++;
            tokens.push_back(str.substr(0, pos));
            str.erase(0, pos + 1);
        }

        if (str.length() > 0) {
            tok++;
            tokens.push_back(str);
        }

        if (tok) {
            return 0;
        } else {
            return -1;
        }
    }

    void MasterModule::build_dali_topology()
    {
        rapidjson::StringBuffer s;
        rapidjson::Writer<rapidjson::StringBuffer> writer(s);
        dev_status_t st;

        writer.StartObject();
        writer.Key("devices");
        writer.StartArray();

        for(auto& elem : devices)
        {
            writer.StartObject();
            
            writer.Key("type");
            writer.String(type_str[(int)elem->get_type()]);

            writer.Key("id");
            writer.Int(elem->get_id());

            st = elem->get_status();
            writer.Key("status");
            writer.StartArray();
            
            writer.Key("on");
            writer.Int(st.status);
            writer.Key("err");
            writer.Int(st.err);
            
            writer.EndArray();
            writer.EndObject();
        }

        writer.EndArray();
        writer.EndObject();

        topology = s.GetString();
    }

    Response MasterModule::get_dali_topology(const Pistache::Rest::Request& req)
    {
        return RestServer::Response(Response::HttpStatus::Ok, topology);
    }

    Response MasterModule::get_dali_led(const Pistache::Rest::Request& req)
    {
        rapidjson::StringBuffer s;
        rapidjson::Writer<rapidjson::StringBuffer> writer(s);
        std::string param;
        std::vector<std::string> ids;

        if (extract_id(req.query().as_str(), param) < 0) {
            return RestServer::Response(Response::HttpStatus::Not_Found, "Missing id param");
        }

        if (split_str(param, ids, ',') < 0) {
            return RestServer::Response(Response::HttpStatus::Not_Found, "Missing ids");
        }

        writer.StartObject();
        writer.Key("leds");
        writer.StartArray();
        for (auto &elem : ids)
        {
            if (elem.compare("all") == 0) {
                for (auto &dev : devices)
                {
                    int id = dev->get_id() ;
                    writer.StartObject();

                    writer.Key("id");
                    writer.Int(id + 1);

                    writer.Key("status");
                    writer.Int(devices[id]->get_status().status);

                    writer.EndObject();
                }
                break;
            } else {
                int id = std::stoi(elem);

                if (id > 0 && id <= device_count) {

                    writer.StartObject();

                    writer.Key("id");
                    writer.Int(id);

                    writer.Key("status");
                    writer.Int(devices[id - 1]->get_status().status);

                    writer.EndObject();
                } else {
                    std::string err = "Led with id : " + elem + " do not exist";
                    return RestServer::Response(Response::HttpStatus::Not_Found, err);
                }
            }
        }

        writer.EndArray();
        writer.EndObject();

        // std::cout << "Query : " << req.query().as_str() << ", id : " << param << std::endl;

        return RestServer::Response(Response::HttpStatus::Ok, s.GetString());
    }

    RestServer::Response MasterModule::post_dali_led_on(const Pistache::Rest::Request& req)
    {
        std::string param;
        std::vector<std::string> ids;

        if (extract_id(req.query().as_str(), param) < 0) {
            return RestServer::Response(Response::HttpStatus::Not_Found, "Missing id param");
        }

        if (split_str(param, ids, ',') < 0) {
            return RestServer::Response(Response::HttpStatus::Not_Found, "Missing ids");
        }

        for (auto &elem : ids)
        {
            if (elem.compare("all") == 0) {
                update_all_leds(1);
                break;
            } else {
                int id = std::stoi(elem);
                Led* led;

                if (id > 0 && id <= device_count) {
                    if (devices[id - 1]->get_type() == LED) {
                        led = (Led*)devices[id - 1];
                        led->set_status(1);
                    }
                } else {
                    std::string err = "Led with id : " + elem + " do not exist";
                    return RestServer::Response(Response::HttpStatus::Not_Found, err);
                }
            }
        }
        return RestServer::Response(Response::HttpStatus::Ok, "");
    }

    RestServer::Response MasterModule::post_dali_led_off(const Pistache::Rest::Request& req)
    {
        std::string param;
        std::vector<std::string> ids;

        if (extract_id(req.query().as_str(), param) < 0) {
            return RestServer::Response(Response::HttpStatus::Not_Found, "Missing id param");
        }

        if (split_str(param, ids, ',') < 0) {
            return RestServer::Response(Response::HttpStatus::Not_Found, "Missing ids");
        }

        for (auto &elem : ids)
        {
            if (elem.compare("all") == 0) {
                update_all_leds(0);
                break;
            } else {
                int id = std::stoi(elem);
                Led* led;

                if (id > 0 && id <= device_count) {
                    if (devices[id - 1]->get_type() == LED) {
                        led = (Led*)devices[id - 1];
                        led->set_status(0);
                    }
                } else {
                    std::string err = "Led with id : " + elem + " do not exist";
                    return RestServer::Response(Response::HttpStatus::Not_Found, err);
                }
            }
        }
        return RestServer::Response(Response::HttpStatus::Ok, "");
    }

    RestServer::Response MasterModule::post_dali_led_toggle(const Pistache::Rest::Request& req)
    {
        std::string param;
        std::vector<std::string> ids;

        if (extract_id(req.query().as_str(), param) < 0) {
            return RestServer::Response(Response::HttpStatus::Not_Found, "Missing id param");
        }

        if (split_str(param, ids, ',') < 0) {
            return RestServer::Response(Response::HttpStatus::Not_Found, "Missing ids");
        }

        for (auto &elem : ids)
        {
            if (elem.compare("all") == 0) {
                toggle_all();
                break;
            } else {
                int id = std::stoi(elem);
                Led* led;

                if (id > 0 && id <= device_count) {
                    if (devices[id - 1]->get_type() == LED) {
                        led = (Led*)devices[id - 1];
                        led->toggle();
                    }
                } else {
                    std::string err = "Led with id : " + elem + " do not exist";
                    return RestServer::Response(Response::HttpStatus::Not_Found, err);
                }
            }
        }
        return RestServer::Response(Response::HttpStatus::Ok, "");
    }


} //end namespace