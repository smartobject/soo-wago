/*
 * Copyright (C) 2021-2022 Mattia Gallacchi <mattia.gallacchi@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#pragma once
#include <cstdio>

#define MAX_CHAR_STR            256

#ifdef DEBUG

#define DBG(fmt, ...) fprintf(stdout, "[%d : %s] " fmt "\n", __LINE__, __func__, ##__VA_ARGS__)

#else

#define DBG(fmt, ...)
#endif

#define YELLOW  "\033[33;1m"
#define RED     "\033[31;1m"
#define BLUE    "\033[34;1m"
#define RESET   "\033[0m"

#define WARN(fmt, ...) fprintf(stdout, YELLOW "[WARNING] " fmt RESET "\n", ##__VA_ARGS__)
#define ERR(fmt, ...) fprintf(stdout, RED "[ERROR] " fmt RESET "\n", ##__VA_ARGS__)