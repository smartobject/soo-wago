/*
 * Copyright (C) 2021-2022 Mattia Gallacchi <mattia.gallacchi@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#pragma once

#include <vector>
#include <thread>
#include <condition_variable>
#include <mutex>

#include "kbusdevice.h"
#include "dalidevice.h"
#include "server.h"

#define PROC_IMAGE_SIZE             24
#define TOGGLE_BUTTON_MODE          0x8

#define STATUS_BYTE                 0x0
#define STATUS_BUTTON_MODE_BIT      (1 << 0)
#define STATUS_BROADCAST_BIT        (1 << 2)
#define TOGGLE_ALL_ON               0x1
#define TOGGLE_ALL_OFF              0x2
#define TOGGLE_BUTTON_MODE          0x8
#define TOGGLE_WATCHDOG             0x10

#define MAX_DEVICES                 0x40 // 64 devices per dali bus
#define DEVICES_BYTE_OFFS           0x2
#define DEVICES_PER_BYTE            0x4
#define DEVICE_STATUS               (1 << 0)
#define DEVICE_ERR                  (1 << 1)
#define NEXT_DEVICE                 0x2
#define ALL_DEVICE_ON               0xAA
#define SCAN_DEVICES_WAIT           0x20
#define MAX_SCANS                   0x5

/* Rest server routes */
#define GET_TOPOLOGY                "/dali/topology"
#define GET_LED                     "/dali/led"
#define POST_LED_ON                 "/dali/led/on"
#define POST_LED_OFF                "/dali/led/off"
#define POST_LED_TOGGLE             "/dali/led/toggle"

#if 0
#define TERM_TASK
#endif

namespace KBUS {

    namespace DALI {

        /**
         * @brief Button mode for dali easy mode. 
         * 
         */
        enum BUTTON_MODE {ONE_BUTTON = 0, TWO_BUTTON};

        /**
         * @brief States for scanning FSM 
         * 
         */
        enum SCAN_STATE {PRE_INIT = 0, INIT, WAIT_DATA, CHECK_DATA, DONE};

        /**
         * @brief Status struct for dali easy mode byte 0 
         * 
         */
        struct status_t {
            BUTTON_MODE mode;
            bool broadcast;
        };

        /**
         * @brief Dali Master Module class
         * 
         */
        class MasterModule : public KbusDevice 
        {
            private:
                // List of detected dali devices. Leds, Switches, etc...
                std::vector<GenericDevice*> devices;

                // Thread to modify dali devices status
                std::thread led_thread;

                // Number of connected dali devices
                int device_count;

                // General status. Byte 0
                status_t status;

                // End of thread task
                bool task_end;

                std::string topology;

                RestServer::Server rest_server;


                // Rest server routes
                RestServer::Response get_dali_topology(const Pistache::Rest::Request& req);

                RestServer::Response get_dali_led(const Pistache::Rest::Request& req);

                RestServer::Response post_dali_led_on(const Pistache::Rest::Request& req);

                RestServer::Response post_dali_led_off(const Pistache::Rest::Request& req);

                RestServer::Response post_dali_led_toggle(const Pistache::Rest::Request& req);


                void build_dali_topology();

                /**
                 * @brief Thread function. 
                 * 
                 */
                void update_task();

                /**
                 * @brief Update byte 0 status. Changes button mode
                 * 
                 */
                void update_status();

                /**
                 * @brief Updates connected devices according to in/out process data
                 * 
                 */
                void update_devices();

                /**
                 * @brief Changes the status of all leds
                 * 
                 * @param status 1 on, 0 off
                 */
                void update_all_leds(int status);

                /**
                 * @brief Get the button mode.
                 * 
                 * @return 0 : 1 Button, 1 : 2 Buttons
                 */
                int get_button_mode();

                /**
                 * @brief Get the broacast status
                 * 
                 * @return 0 : off, 1 : on
                 */
                int get_broacast_status();

                /**
                 * @brief Scan dali bus add devices to devices array. Sets device_count
                 * 
                 * @return true on success, false otherwise
                 */
                bool scan_devices();

                // Synchro of update_task
                std::condition_variable cv;
                std::mutex cv_mutex;

                // Mutex used to protect shared variables access
                std::mutex var_acces;

            public:

                /**
                 * @brief Construct a new Master Module object
                 * 
                 * @param pos Bus position
                 * @param in_offs_bytes Offset on the in process data array
                 * @param out_offs_bytes Offset on the out process data array
                 */
                MasterModule(int pos, uint16_t in_offs_bytes, uint16_t out_offs_bytes);

                /**
                 * @brief Destroy the Master Module object
                 * 
                 */
                ~MasterModule();
                
                /**
                 * @brief Process data read from the bus and write data to be sended back.
                 * 
                 * @param in Process data input
                 * @param out Process data output
                 */
                void process_images(uint8_t *in, uint8_t *out);

                /**
                 * @brief Perform action before quitting execution
                 * 
                 * @param in Process data input
                 * @param out Process data output
                 */                
                void process_end(uint8_t *in, uint8_t *out);

                /**
                 * @brief Add a new led device to the devices array
                 * 
                 */
                void add_led(int byte_num, int bit_pos);

                /**
                 * @brief Start a thread. It can be used for multiple pourpouses
                 * 
                 */
                void thread_start();

                /**
                 * @brief Stop running thread
                 * 
                 */                
                void thread_end();

                /**
                 * @brief Prints the process data input. Used for debug
                 * 
                 */
                void print_input();

                /**
                 * @brief Set the button mode 
                 * 
                 * @param mode 1 button, 2 buttons
                 */
                void set_button_mode(BUTTON_MODE mode);

                /**
                 * @brief Toggle all leds. Invert current the state
                 * 
                 */
                void toggle_all();
        };
    }// end DALI
}// end KBUS