#include <iostream>

#include "dalidevice.h"

namespace KBUS {
    namespace DALI {

        GenericDevice::GenericDevice(TYPE type, int id, int byte_num, int bit_pos) :
        m_type(type),
        m_id(id)
        {
            std::cout << "New Dali device added :\n\tType : " << type_str[m_type]
                << "\n\tID : " << m_id << std::endl;
            m_data_pos = {byte_num, bit_pos};
            changing = false;
        }

        GenericDevice::~GenericDevice()
        {
            
        }

        TYPE GenericDevice::get_type()
        {
            return m_type;
        }

        data_pos_t GenericDevice::get_data_pos()
        {
            return m_data_pos;
        }

        int GenericDevice::get_id()
        {
            return m_id;
        }

        dev_status_t GenericDevice::get_status()
        {
            return m_status;
        }

    }// end DALI
}// end KBUS