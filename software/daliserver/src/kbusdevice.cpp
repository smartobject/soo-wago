/*
 * Copyright (C) 2021-2022 Mattia Gallacchi <mattia.gallacchi@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <iostream>

#include "kbusdevice.h"

namespace KBUS {

    KbusDevice::KbusDevice(int pos, DEVICE_TYPE type, uint16_t in_offs_bytes, uint16_t out_offs_bytes,
                            uint16_t size_input_bytes, uint16_t size_output_bytes) 
    :
        m_device_pos(pos),
        m_type(type),
        m_in_offs_bytes(in_offs_bytes), 
        m_out_offs_bytes(out_offs_bytes)
    {
        DBG("Add  kbus device");
        DBG("In offset start at %d", m_in_offs_bytes);
        DBG("Out offset start at %d", m_out_offs_bytes);
        out_proc_images = std::vector<uint8_t>(size_output_bytes, 0x0);
        in_proc_images = std::vector<uint8_t>(size_input_bytes, 0x0);
    }

    KbusDevice::~KbusDevice()
    {
        DBG("KbusDevice destructror");
    }

    void KbusDevice::print_info()
    {
        std::cout << "\tModule name : " << device_desc[m_type] << std::endl;
        std::cout << "\tDevice position : " << m_device_pos << std::endl;
        std::cout << "\tInput offest : " << m_in_offs_bytes << std::endl;
        std::cout << "\tOutput offset : " << m_out_offs_bytes << std::endl;
    }

    int KbusDevice::get_pos()
    {
        return m_device_pos;
    }

    DEVICE_TYPE KbusDevice::get_type()
    {
        return m_type;
    }


}