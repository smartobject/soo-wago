/*
 * Copyright (C) 2021-2022 Mattia Gallacchi <mattia.gallacchi@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <stdexcept>

#include <unistd.h>
#include <csignal>

#include "kbus.h"
#include "common.h"

// priorities
#define KBUS_MAINPRIO 40       // main loop
// runtime of the test
#define KBUS_TESTTIME 30

namespace KBUS {
    
    // static bool loop_end = false;
    bool KbusMaster::loop_end = false;

    void KbusMaster::sig_handler(int signo)
    {
        if (signo == SIGINT) {
            loop_end = true;
        }
    }

    KbusMaster::KbusMaster()
    {
        size_t i = 0;
        struct sched_param s_param;

        if (std::signal(SIGINT, sig_handler) == SIG_ERR) {
            std::runtime_error("Failed to set callback for signals");
        }

        memset(pd_in, 0, sizeof(pd_in));
        memset(pd_out, 0, sizeof(pd_out));

        adi = adi_GetApplicationInterface();
        adi->Init();

        adi->ScanDevices();
        adi->GetDeviceList(sizeof(deviceList), deviceList, &nrDevicesFound);

        // find kbus device
        nrKbusFound = -1;
        for (i = 0; i < nrDevicesFound; ++i)
        {
            if (strcmp(deviceList[i].DeviceName, "libpackbus") == 0)
            {
                nrKbusFound = i;
                DBG("KBUS device found as device %i", i);
            }
        }

        // kbus not found > exit
        if (nrKbusFound == -1)
        {
            adi->Exit(); // disconnect ADI-Interface
            throw std::runtime_error("No KBUS device found");
        }

        // switch to RT Priority
        s_param.sched_priority = KBUS_MAINPRIO;
        sched_setscheduler(0, SCHED_FIFO, &s_param);
        DBG("switch to RT Priority 'KBUS_MAINPRIO'");

        // open kbus device
        kbusDeviceId = deviceList[nrKbusFound].DeviceId;
        if (adi->OpenDevice(kbusDeviceId) != DAL_SUCCESS)
        {
            adi->Exit(); // disconnect ADI-Interface
            throw std::runtime_error("Kbus device open failed");
        }
        DBG("KBUS device open OK");

        DBG("Scanning the bus...");
        if (get_bus_info() < 0) {
            adi->Exit();
            throw std::runtime_error("Failed to scan the kbus");
        }
        DBG("Bus scan successfull");

        DBG("Getting connected terminals informations");
        if (get_terminals_info() < 0) {
            adi->Exit();
            throw std::runtime_error("Failed to get terminals infos");
        }
        DBG( "Terminals informations gathering successfull");

        /* Add supported device to the device list */
        add_all_bus_devices();

    }

    KbusMaster::~KbusMaster()
    {
        DBG("Destroying KBusMaster");

        for (std::list<KbusDevice*>::iterator iter = device_list.begin(); 
                iter != device_list.end(); ++iter)
        {
            (*iter)->thread_end();
            delete *iter;
        }

        if(adi->IsDeviceOpen(kbusDeviceId))
            adi->CloseDevice(kbusDeviceId); // close kbus device

        adi->Exit(); // disconnect ADI-Interface
    }

    int KbusMaster::get_bus_info()
    {
        event.State = ApplicationState_Unconfigured;
        if (adi->ApplicationStateChanged(event) != DAL_SUCCESS)
        {
            // Set application state to "Unconfigured" failed
            ERR("Set application state to 'Unconfigured' failed");
            return -1; // exit programm
        }
        DBG("Set application state to 'Unconfigured'");

        //*** Retrieve KBUS-Config via DBUS ***************************
        if (KbusInfo_Failed == ldkc_KbusInfo_Create())
        {
            ERR("ldkc_KbusInfo_Create() failed");
            return -1;
        }

        // GetStatus
        if (KbusInfo_Failed == ldkc_KbusInfo_GetStatus(&status))
        {
            ERR(" ldkc_KbusInfo_GetStatus() failed");
            ldkc_KbusInfo_Destroy();
            return -1;
        }

        ldkc_KbusInfo_Destroy();
        return 0;
    }

    void KbusMaster::print_bus_info()
    {
        std::cout << "Kbus status: " << std::endl;
        std::cout << "\tBit count : " << status.KbusBitCount << std::endl;
        std::cout << "\tTerminal count : " << status.TerminalCount << std::endl;
        std::cout << "\tError code : " << status.ErrorCode << std::endl;
        std::cout << "\tError arg : " << status.ErrorArg << std::endl;
        std::cout << "\tError pos : " << status.ErrorPos << std::endl;
        std::cout << "\tBit analog in count : " << status.BitCountAnalogInput << std::endl;
        std::cout << "\tBit analog out count : " << status.BitCountAnalogOutput << std::endl;
        std::cout << "\tBit digital in count : " << status.BitCountDigitalInput << std::endl;
        std::cout << "\tBit digital out count : " << status.BitCountDigitalOutput << std::endl;
    }

    int KbusMaster::get_terminals_info()
    {
        event.State = ApplicationState_Unconfigured;
        if (adi->ApplicationStateChanged(event) != DAL_SUCCESS)
        {
            // Set application state to "Unconfigured" failed
            ERR("Set application state to 'Unconfigured' failed");
            return -1; // exit programm.
        }
        DBG("Set application state to 'Unconfigured'");

        return c_get_terminals_info(terminalDescription, &terminalCount, terminals);
    }

    void KbusMaster::print_terminals_info()
    {
        size_t index;

        for(index = 0; index < terminalCount; index++)
        {
            std::cout << "Pos " << index << ", Type " << terminals[index] << std::endl;
            if (terminals[index] == 647) {
                std::cout << "Found DALI MM 750-647 : " << std::endl;
                std::cout << "\tOffset input bits : " << terminalDescription[index].OffsetInput_bits << std::endl;
                std::cout << "\tOffset output bits : " << terminalDescription[index].OffsetOutput_bits << std::endl;
                std::cout << "\tSize input bits : " << terminalDescription[index].SizeInput_bits << std::endl;
                std::cout << "\tSize output bits : " << terminalDescription[index].SizeOutput_bits << std::endl;
                std::cout << "\tChannel count : " << terminalDescription[index].AdditionalInfo.ChannelCount << std::endl;
                std::cout << "\tPi format : " << terminalDescription[index].AdditionalInfo.PiFormat << std::endl;
            }
        }
    }

    void KbusMaster::add_all_bus_devices()
    {
        std::cout << BLUE "Loaded modules : " << std::endl;

        for (size_t index = 0; index < terminalCount; index++) 
        { 
            add_device(terminals[index]);
            std::cout << std::endl;
        }

        std::cout << RESET << std::endl;
    }

    bool KbusMaster::search_device(int type, uint16_t& in_offs_bytes, 
                                    uint16_t& out_offs_bytes, uint16_t& pos)
    {
        size_t index;
        bool rc = false;

        for (index = 0; index < terminalCount; index++) {
            if (terminals[index] == type) {
                in_offs_bytes = terminalDescription[index].OffsetInput_bits;
                out_offs_bytes = terminalDescription[index].OffsetOutput_bits;
                pos = index;
                rc = true;
            }
        }  

        return rc;
    }

    void KbusMaster::add_device(int type)
    {
        uint16_t in_offs, out_offs, pos;
        KbusDevice *newDevice = nullptr;

        if (!search_device(type, in_offs, out_offs, pos)) {
            throw std::runtime_error("Could not find device type : " + std::to_string(type));
        }

        switch (type)
        {
        case MODULE_DALI_MM:
            newDevice = new DALI::MasterModule(pos, in_offs, out_offs);
            device_list.push_back(newDevice);
            break;
        case MODULE_16_DIGITAL_OUT:
            WARN("%s not yet implemented", device_desc[DIGITAL_16OUT]);
            break;
        default:
            throw std::runtime_error("Device " + std::to_string(type) + " not defined");
            break;
        }

        if (newDevice != nullptr) {
            newDevice->print_info();
            newDevice->thread_start();
        } 
    }

    KbusDevice* KbusMaster::get_device(DEVICE_TYPE type, int pos)
    {
        for(auto &elem: device_list)
        {
            if (elem->get_pos() == pos){
                if (elem->get_type() == type){
                    return elem;
                }
            }
        }

        return nullptr;
    }

    void KbusMaster::devices_proc()
    {
        for (std::list<KbusDevice*>::iterator iter = device_list.begin(); 
                iter != device_list.end(); ++iter)
        {
            (*iter)->process_images(pd_in, pd_out);
        }
    }

    void KbusMaster::devices_end()
    {
        for (std::list<KbusDevice*>::iterator iter = device_list.begin(); 
                iter != device_list.end(); ++iter)
        {
            (*iter)->process_end(pd_in, pd_out);
        }
    }

    void KbusMaster::read_bus()
    {
        if (adi->ReadStart(kbusDeviceId, taskId) < 0) {
            throw std::runtime_error("Read start");
        } 
            
        if (adi->ReadBytes(kbusDeviceId, taskId, 0x0, MAX_PD_NR, pd_in) < 0) {
            throw std::runtime_error("Read bytes");
        } 

        if (adi->ReadEnd(kbusDeviceId, taskId) < 0) {
            throw std::runtime_error("Read stop");
        }
    }

    void KbusMaster::write_bus()
    {
        if (adi->WriteStart(kbusDeviceId, taskId) < 0) {
            throw std::runtime_error("Read start");
        } 
            
        if (adi->WriteBytes(kbusDeviceId, taskId, 0x0, MAX_PD_NR, pd_out) < 0) {
            throw std::runtime_error("Read bytes");
        } 

        if (adi->WriteEnd(kbusDeviceId, taskId) < 0) {
            throw std::runtime_error("Read stop");
        }
    }

    void KbusMaster::trigger_bus_cycle()
    {
        uint32_t retval = 0;

                    // Use function "libpackbus_Push" to trigger one KBUS cycle.
        if (adi->CallDeviceSpecificFunction("libpackbus_Push", &retval) != DAL_SUCCESS)
        {
            adi->CloseDevice(kbusDeviceId); // close kbus device
            adi->Exit(); // disconnect ADI-Interface
            throw std::runtime_error("CallDeviceSpecificFunction failed");
        }

        if (retval != DAL_SUCCESS)
        {
            adi->CloseDevice(kbusDeviceId); // close kbus device
            adi->Exit(); // disconnect ADI-Interface
            throw std::runtime_error("Function 'libpackbus_Push' failed");
        }
    }

    void KbusMaster::bus_loop()
    {
        int final_loop = FINAL_LOOP_ITER;
        taskId = 0;

        DBG("Starting KBUS loop");

        // Set application state to "Running" to drive kbus by your selve.
        event.State = ApplicationState_Running;
        if (adi->ApplicationStateChanged(event) != DAL_SUCCESS)
        {
            // Set application state to "Running" failed
            adi->CloseDevice(kbusDeviceId); // close kbus device
            adi->Exit(); // disconnect ADI-Interface
            throw std::runtime_error("Set application state to 'Running' failed");
        }

        while(!loop_end) 
        {
            usleep(10000);
            
            trigger_bus_cycle();

            read_bus();

            /* Perform all devices operation with the read bus values */
            devices_proc();

            write_bus();
        }

        while(final_loop) {

            usleep(10000);
            trigger_bus_cycle();

            read_bus();

            devices_end();

            write_bus();

            final_loop--;
        }
    }
} // KBUS