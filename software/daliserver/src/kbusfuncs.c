#include <stdio.h>

#include "kbus.h"

int c_get_terminals_info(tldkc_KbusInfo_TerminalInfo *termInfo, size_t *termCount, u16 *terms)
{
    if (KbusInfo_Failed == ldkc_KbusInfo_Create())
    {
        printf(" ldkc_KbusInfo_Create() failed\n");
        return -1;
    }

    if (ldkc_KbusInfo_GetTerminalInfo(LDKC_KBUS_TERMINAL_COUNT_MAX, termInfo, termCount) < 0)
    {
        printf(" ldkc_KbusInfo_GetTerminalInfo() failed\n");
        ldkc_KbusInfo_Destroy();
        return -1;
    }

    if ( KbusInfo_Failed == ldkc_KbusInfo_GetTerminalList(LDKC_KBUS_TERMINAL_COUNT_MAX, terms, NULL) )
    {
        printf(" ldkc_KbusInfo_GetTerminalList() failed\n");
        ldkc_KbusInfo_Destroy();
        return -1;
    }

    ldkc_KbusInfo_Destroy();
    return 0;
}