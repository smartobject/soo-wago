/*
 * Copyright (C) 2021-2022 Mattia Gallacchi <mattia.gallacchi@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#pragma once


#ifdef __cplusplus

extern "C" {
#include <dal/adi_application_interface.h>
#include <ldkc_kbus_information.h>
#include <ldkc_kbus_register_communication.h>

int c_get_terminals_info(tldkc_KbusInfo_TerminalInfo *termInfo, size_t *termCount, u16 *terms);
}
#else

#include <dal/adi_application_interface.h>
#include <ldkc_kbus_information.h>
#include <ldkc_kbus_register_communication.h>

int c_get_terminals_info(tldkc_KbusInfo_TerminalInfo *termInfo, size_t *termCount, u16 *terms);

#endif

#define NR_DEVICES      256
#define MAX_PD_NR       4096
#define BYTE_SIZE       8
#define FINAL_LOOP_ITER 10

#ifdef __cplusplus

#include <list>

#include "dalimaster.h"

namespace KBUS {

    /**
     * @brief   This class is used to interact with the wago kbus. It's pourpouse is to initialize 
     *          and manage all kbus devices. It will auto scan the bus and add each device found.
     */
    class KbusMaster
    {
        private:
            // Bus devices list
            tDeviceInfo deviceList[NR_DEVICES];

            // number of devices found
            size_t nrDevicesFound;

            //position of the kbus in the device list
            int nrKbusFound;              

            // Device ID from the ADI
            tDeviceId kbusDeviceId;

            // Pointer to the application interface           
            tApplicationDeviceInterface * adi;

            // Task Id 
            uint32_t taskId = 0;

            // var for the event interface of the ADI
            tApplicationStateChangedEvent event;

            // Kbus status
            tldkc_KbusInfo_Status status;

            // List of connenected I/O modules
            u16 terminals[LDKC_KBUS_TERMINAL_COUNT_MAX];

            // I/O modules descrition struct
            tldkc_KbusInfo_TerminalInfo terminalDescription[LDKC_KBUS_TERMINAL_COUNT_MAX];

            // Number of I/O connected modules
            size_t terminalCount;
            
            /** process data **/
            // kbus input process data
            uint8_t pd_in[MAX_PD_NR];

            // kbus output process data    
            uint8_t pd_out[MAX_PD_NR];   
            
            // I/O Modules list
            std::list<KbusDevice*> device_list;

            // Main bus loop end
            static bool loop_end;

            /**
             * @brief Get the Bus status struct filled with data
             * 
             * @return 0 on success, -1 on error
             */
            int get_bus_info();

            /**
             * @brief Get the I/O modules general infos
             * 
             * @return 0 on success, -1 on error
             */
            int get_terminals_info();

            /**
             * @brief Iterate through I/O module in device_list and calls process_images() method
             * 
             */
            void devices_proc();
            
            /**
             * @brief Iterate through I/O module in device_list and calls process_end() method
             * 
             */
            void devices_end();
            
            /**
             * @brief Add all found and supported I/O modules to device_list
             * 
             */
            void add_all_bus_devices();

            /**
             * @brief Search for a specific device in the terminals array
             * 
             * @param type Type of the device. Types are defined in header kbusdevice.h
             * @param in_offs_bytes Input process data offeset
             * @param out_offs_bytes Output process data offset
             * @param pos Position on the kbus
             * @return true if device found, false otherwise
             */
            bool search_device(int type, uint16_t& in_offs_bytes, 
                                uint16_t& out_offs_bytes, uint16_t& pos);

            /**
             * @brief Adds a I/O module to device_list
             * 
             * @param type Type of the device. Types are defined in header kbusdevice.h
             */
            void add_device(int type);

            /**
             * @brief   Read bus process data and fill the pd_in array.
             *          Throw std::runtime_error if it fails.
             * 
             */
            void read_bus();

            /**
             * @brief   Write bus process data from the pd_out array
             *          Throw std::runtime_error if it fails.
             * 
             */
            void write_bus();

            /**
             * @brief   Start a new read/write bus cycle
             *          Throw std::runtime_error if it fails.
             * 
             */
            void trigger_bus_cycle();

            /**
             * @brief Signal handler to trigger end of programm
             * 
             * @param signo 
             */
            static void sig_handler(int signo);

        public: 
            /**
             * @brief Construct a new Kbus Master object
             * 
             */
            KbusMaster();

            /**
             * @brief Destroy the Kbus Master object
             * 
             */
            ~KbusMaster();

            /**
             * @brief Print the bus info contained in the status struct
             * 
             */
            void print_bus_info();

            /**
             * @brief Print the I/O modules info contained in the terminalDescription array
             * 
             */
            void print_terminals_info();

            /**
             * @brief Bus loop reading, processing and writing process data
             * 
             */
            void bus_loop();

            /**
             * @brief Get the a device object from the device_list
             * 
             * @param type Type of the device. Types are defined in header kbusdevice.h
             * @param pos Position on the bus
             * @return KbusDevice object on success, nullptr on error
             */
            KbusDevice *get_device(DEVICE_TYPE type, int pos);
    };
} // KBUS
#endif