/*
 * Copyright (C) 2021-2022 Mattia Gallacchi <mattia.gallacchi@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#pragma once

#include <vector>
#include <stdint.h>

#include "common.h"

/* Modules IDs */
#define MODULE_DALI_MM          647
#define MODULE_16_DIGITAL_OUT   0x9002

namespace KBUS {
    
    /**
     * @brief Supported I/O modules
     * 
     */
    enum DEVICE_TYPE {
        DALI_MM = 0,
        DIGITAL_16OUT
    };

    static const char device_desc[][MAX_CHAR_STR] = {
        "DALI Multi Master (750-647)",
        "16-Channel digital output (750-1504)"
    };

    /**
     * @brief Base I/O modules class form which each type of I/O module should inherit
     * 
     */
    class KbusDevice
    {

    public:
        /**
         * @brief Process data read from the bus and write data to be sended back.
         * 
         * @param in Process data input
         * @param out Process data output
         */
        virtual void process_images(uint8_t *in, uint8_t *out) = 0;

        /**
         * @brief Perform action before quitting execution
         * 
         * @param in Process data input
         * @param out Process data output
         */
        virtual void process_end(uint8_t *in, uint8_t *out) = 0;

        /**
         * @brief Prints the process data input. Used for debug
         * 
         */
        virtual void print_input() = 0;

        /**
         * @brief Destroy the Kbus Device object
         * 
         */
        virtual ~KbusDevice() = 0;

        /**
         * @brief Start a thread. It can be used for multiple pourpouses
         * 
         */
        virtual void thread_start() = 0;

        /**
         * @brief Stop running thread
         * 
         */
        virtual void thread_end() = 0;

        /**
         * @brief Get the device type
         * 
         * @return DEVICE_TYPE 
         */
        DEVICE_TYPE get_type();

        /**
         * @brief Get the posisition on the bus
         * 
         * @return position 
         */
        int get_pos();

        /**
         * @brief Print I/O module infos
         * 
         */
        void print_info();

    protected:

        // Position on the kbus
        int m_device_pos;

        // I/O module type
        DEVICE_TYPE m_type;

        // Offset on the in process data array
        uint16_t m_in_offs_bytes;

        // Offset on the out process data array
        uint16_t m_out_offs_bytes;

        // Vectors of the module specific process data
        std::vector<uint8_t> out_proc_images;
        std::vector<uint8_t> in_proc_images;
        
        /**
         * @brief Construct a new Kbus Device object
         * 
         * @param pos Position on the bus
         * @param type I/O Module type
         * @param in_offs_bytes Offset on the in process data array
         * @param out_offs_bytes Offset on the out process data array
         * @param size_input_bytes Number of bytes of the input process data
         * @param size_output_bytes Number of bytes of the output process data
         */
        KbusDevice(int pos, DEVICE_TYPE type, uint16_t in_offs_bytes, uint16_t out_offs_bytes, 
                    uint16_t size_input_bytes, uint16_t size_output_bytes);

    };
} //KBUS

 