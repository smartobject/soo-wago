/*
 * Copyright (C) 2021-2022 Mattia Gallacchi <mattia.gallacchi@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <iostream>

#include "dalidevice.h"
#include "common.h"

namespace KBUS {
    namespace DALI {

        Led::Led(int id,  int byte_num, int bit_pos) : GenericDevice(LED, id, byte_num, bit_pos)
        {
            DBG("Add new LED with id %d", id);
            m_status = {OFF, 0};
        }

        Led::~Led()
        {
            DBG("Destroy Led %d", m_id);
        }

        void Led::update_status(dev_status_t& s)
        {
            int status = 0;
            
            if (!changing) {
                if (s.status != m_status.status) {
                    changing = true;
                    status = DEVICE_TOGGLE;
                } 
            } else {
                if (s.status == m_status.status){
                    changing = false;
                }
            }

            s.status = status;
        }

        void Led::toggle()
        {
            m_status.status = !m_status.status;
        }

        void Led::set_status(int status)
        {
            m_status.status = status;
        }

    }// end DALI
}// end KBUS