#include <iostream>
#include <nlohmann/json.hpp>

#include "kbus.h"
#include "dalimaster.h"

int main()
{
    KBUS::KbusMaster kMaster;
    
    // dali->set_button_mode(KBUS::DALI::BUTTON_MODE::TWO_BUTTON);

    kMaster.bus_loop();

    return 0;
}   