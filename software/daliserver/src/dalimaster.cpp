#include <iostream>
#include <string.h>
#include <bitset>
#include <algorithm>

#include "dalimaster.h"
#include "common.h"

namespace KBUS {
    namespace DALI {

        MasterModule::MasterModule(int pos, uint16_t in_offs_bytes, uint16_t out_offs_bytes) :
            KbusDevice(pos, DALI_MM, in_offs_bytes, out_offs_bytes, PROC_IMAGE_SIZE, PROC_IMAGE_SIZE)
        {
            DBG("New DALI MM loaded");
            device_count = 0;
            status = {ONE_BUTTON, 0};
            task_end = false;
        }

        MasterModule::~MasterModule()
        {
            DBG("DALI MM destructor");

            for (std::vector<GenericDevice*>::iterator iter = devices.begin(); 
                iter != devices.end(); ++iter)
            {
                delete *iter;
            }
        }

        void MasterModule::update_task()
        {
            std::string read;
            int r_val;
            bool end = false;
            Led *l = nullptr;
            std::unique_lock<std::mutex> lck(cv_mutex);

            cv.wait(lck);

            while (!end) 
            {
                std::cout << "Enter <led num> to toogle one led\n"
                    << "Press a to toggle all leds, f to turn off all leds, o to turn on all leds" 
                        << std::endl;
                std::cin >> read;
                
                if (read.empty()) {
                    continue;
                } else if (std::all_of(read.begin(), read.end(), ::isdigit)) {
                    r_val = std::stoi(read);
                } else {
                    r_val = (int)read[0];
                }  

                switch (r_val)
                {
                    case 'a':
                        toggle_all();
                        break;

                    case 'o':
                        update_all_leds(1);
                        break;

                    case 'f':
                        update_all_leds(0);
                        break;

                    default:

                        if (r_val > 0 && r_val <= device_count) {
                            if (devices[r_val - 1]->get_type() == LED) {
                                l = (Led*)devices[r_val -1];
                                l->toggle();
                            }
                        } else {
                            std::cout << "CMD " << read << " not implemented" << std::endl;
                        }
                        break;
                }

                var_acces.lock();
                end = task_end;
                var_acces.unlock();
            }
        }

        void MasterModule::thread_start()
        {
#ifdef TERM_TASK
            led_thread = std::thread(&MasterModule::update_task, this);
#else
            rest_server.addRoute(&MasterModule::get_dali_topology, this,
                                RestServer::Route::GET, GET_TOPOLOGY);
            
            rest_server.addRoute(&MasterModule::get_dali_led, this,
                                RestServer::Route::GET, GET_LED);

            rest_server.addRoute(&MasterModule::post_dali_led_on, this, 
                                RestServer::Route::POST, POST_LED_ON);
            
            rest_server.addRoute(&MasterModule::post_dali_led_off, this,
                                RestServer::Route::POST, POST_LED_OFF);

            rest_server.addRoute(&MasterModule::post_dali_led_toggle, this,
                                RestServer::Route::POST, POST_LED_TOGGLE);
            
            rest_server.start();
#endif
        }

        void MasterModule::thread_end()
        {
#ifdef TERM_TASK
            var_acces.lock();
            task_end = true;
            var_acces.unlock();

            DBG("Waiting thread end");

            led_thread.join();
#else
            rest_server.stop();
#endif
        }

        void MasterModule::update_status()
        {
            int mode = 0; 
            static int mode_changed = 0;
            out_proc_images[STATUS_BYTE] = 0x0;

            mode = get_button_mode();

            //Handle mode
            if (status.mode != (BUTTON_MODE)mode && !mode_changed) {
                out_proc_images[STATUS_BYTE] |= TOGGLE_BUTTON_MODE;
                mode_changed = 1;
                std::cout << "Switching button mode" << std::endl;
            } else if (mode_changed) {
                if (status.mode == (BUTTON_MODE)mode) {
                    mode_changed = 0;
                }
            }
        }

        void MasterModule::process_images(uint8_t *in, uint8_t *out)
        {
            static int configured = 0;

            for(int i = 0; i < PROC_IMAGE_SIZE; i++) 
            {
                in_proc_images[i] = in[m_in_offs_bytes + i];
            }

            if (!configured) {
                if (scan_devices()) {
                    configured = 1;
                    cv.notify_all();
                }
            } else {
                update_status();
                update_devices();
            }

            for (int i = 0; i < PROC_IMAGE_SIZE; i++)
            {
                out[m_out_offs_bytes + i] = out_proc_images[i];

                // Reset for next iteration
                out_proc_images[i] = 0;
            }

        }

        void MasterModule::update_devices()
        {
            int data;
            GenericDevice* dev;
            dev_status_t dev_st;
            data_pos_t pos;


            for (int i = 0; i < device_count; i++) 
            {
                dev = devices[i];
                pos = dev->get_data_pos();

                data = in_proc_images[pos.byte] >> pos.bit;
                dev_st = {(int)(data & DEVICE_STATUS), (int)(data & DEVICE_ERR) >> 1};
                dev->update_status(dev_st);

                out_proc_images[pos.byte] |= ((dev_st.status << 1) << pos.bit);
            }
        }

        bool MasterModule::scan_devices()
        {
            uint8_t in = 0;
            static int step = PRE_INIT;
            static int failed_scans = 0;
            static int wait_loop = 0;
            bool ret = false;

            switch (step)
            {
                case PRE_INIT:
                    if (wait_loop == 0) {
                        out_proc_images[STATUS_BYTE] = TOGGLE_ALL_OFF;
                    } else if (wait_loop > SCAN_DEVICES_WAIT) {
                        step = INIT;
                        wait_loop = 0;
                    }

                    wait_loop++;
                    
                    break;

                case INIT:
                    out_proc_images[STATUS_BYTE] = TOGGLE_ALL_ON;
                    step = WAIT_DATA;
                    break;
                
                case WAIT_DATA:
                    if (wait_loop > SCAN_DEVICES_WAIT) {
                        step = CHECK_DATA;
                        wait_loop = 0;
                    } else
                        wait_loop++;
                    break;

                case CHECK_DATA:

                    for (size_t i = 0; i < MAX_DEVICES / DEVICES_PER_BYTE; i++) {
                        in = in_proc_images[i + DEVICES_BYTE_OFFS];

                        for (size_t j = 0; j < DEVICES_PER_BYTE; j++)
                        {
                            int on = (int)(in & DEVICE_STATUS);
                            int err = (int)(in & DEVICE_ERR) >> 1;

                            DBG("[%d] ON/OFF : %d\t\tError : %d\n", index, on, err);

                            if (on) {
                                if(!err) {
                                    add_led(i + DEVICES_BYTE_OFFS, 2 * j);
                                } else {
                                    std::cout << "Error : Device " << device_count + 1 << 
                                            " is in error state" << std::endl;
                                }
                            }

                            in >>= NEXT_DEVICE;
                        }
                    }
                    step = DONE;

                    break;

                case DONE:
                    if (device_count) {
                        std::cout << "DALI devices scan end. Found " << device_count 
                            << " devices" << std::endl;
                        out_proc_images[STATUS_BYTE] = TOGGLE_ALL_OFF;
                        ret = true;
                        build_dali_topology();
                    } else {
                        failed_scans++;
                        if (failed_scans > MAX_SCANS) {
                            std::runtime_error("No devices where found on the DALI bus");
                        }
                        step = INIT;
                    }
                    
                    break;
                default:
                    break;
            } 

            return ret;
        }

        void MasterModule::set_button_mode(BUTTON_MODE mode)
        {
            var_acces.lock();
            status.mode = mode;
            var_acces.unlock();
        }

        void MasterModule::toggle_all()
        {
            Led *led = nullptr;

            for (int i = 0; i < device_count; i++)
            {
                if (devices[i]->get_type() == LED) {
                    led = (Led*)devices[i];

                    if (led) led->toggle();
                }
            }
        }

        int MasterModule::get_button_mode()
        {
            return in_proc_images[STATUS_BYTE] & STATUS_BUTTON_MODE_BIT;
        }

        int MasterModule::get_broacast_status()
        {
            return ((in_proc_images[STATUS_BYTE] & STATUS_BROADCAST_BIT) >> 2);
        }

        void MasterModule::print_input()
        {
            for (int i = 0; i < 1; i++)
            {
                std::cout << "[" << i << "] in : " << std::hex
                        << (int)in_proc_images[i] << "\t==>" 
                        << "\t out : " << (int)out_proc_images[i] 
                        << std::dec << std::endl;
            }
        }

        void MasterModule::add_led(int byte_num, int bit_pos)
        {
            devices.push_back(new Led(device_count, byte_num, bit_pos));
            device_count++;
        }

        void MasterModule::update_all_leds(int status)
        {
            Led *led = nullptr;

            for (int i = 0; i < device_count; i++)
            {
                if (devices[i]->get_type() == LED) {
                    led = (Led*)devices[i];

                    if (led) led->set_status(status);
                }
            }
        }

        void MasterModule::process_end(uint8_t *in, uint8_t *out)
        {
            for(int i = 0; i < PROC_IMAGE_SIZE; i++) 
            {
                in_proc_images[i] = in[m_in_offs_bytes + i];
            }

            update_all_leds(0);
            update_devices();

            for (int i = 0; i < PROC_IMAGE_SIZE; i++)
            {
                out[m_out_offs_bytes + i] = out_proc_images[i];

                // Reset for next iteration
                out_proc_images[i] = 0;
            }
        }

    } //end DALI
} //end KBUS