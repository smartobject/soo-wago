#!/bin/sh

PROJECT_DIR="../../pfc-firmware-sdk-G2"
PKG_NAME="daliserver"
SRC_PATH=$(pwd)
FILE=.configured

copy_rules()
{
    echo "Copying project rules to ${PROJECT_DIR}"
    cp rules/* $PROJECT_DIR/rules/
}

copy_src()
{
    mkdir -p $PROJECT_DIR/src/$PKG_NAME
    cp src/* $PROJECT_DIR/src/$PKG_NAME/
}

compile_app()
{
    cd $PROJECT_DIR
    ptxdist compile $PKG_NAME
    cd $SRC_PATH
}

clean_app()
{
    cd $PROJECT_DIR
    ptxdist clean $PKG_NAME
    rm -rf src/${PKG_NAME}
    cd $SRC_PATH
}

generate_pkg()
{
    cd $PROJECT_DIR
    ptxdist targetinstall $PKG_NAME
    cd $SRC_PATH
}

first()
{
    if test -f "$FILE"; then
        echo "Project already configured"
    else
        cp -r rules/* $PROJECT_DIR/rules

        cd $PROJECT_DIR
        ptxdist menuconfig
        ptxdist local-src -f daliserver $SRC_PATH/src
        cd $SRC_PATH

        touch $FILE
    fi
}

check_config()
{
    if test -f "$FILE"; then
        break
    else
        echo "Use -f option to configure the project"
        exit
    fi
}

help()
{
    echo "Allows to compile a package"
    echo
    echo "Syntax: [-c|f|h]"
    echo "Options: "
    echo "b     Copiles the source code"
    echo "c     Clean project"
    echo "f     First time configuration"
    echo "g     Generate pacakge"
    echo "h     Prints this help"
}

if [ $# -eq 0 ];
then
    echo "Error: missing argument" 
    help
    exit 0
else

while getopts ":bcfgh" option; do
    case $option in
        b)
            check_config
            clean_app
            copy_src
            compile_app
            exit;;
        c)
            clean_app
            exit;;

        f)  
            first
            exit;;
        g)
            check_config
            generate_pkg
            exit;;
        h)
            help
            exit;;
        \?)
            echo "Error: invalid option"
            help
            exit;;
    esac
done
fi