# PCF200 Configuration

This guide Will explain how to configure for the first time the PCF200 (750-8212) controller. Note : You'll need a Windows machine to configure a static IP address.

## Configure IP address and connect to the PFC200
First download the Wago [ Ethernet settings software](https://www.wago.com/global/d/445) and install it.
Then you can either connect te PLC200 through the wago [USB configurator cable](https://www.wago.com/global/accessories/configuration-cable/p/750-923) or using an ethernet cable. If you choose the second option you have to press the reset button for 8s until the SYS led flashes orange. This action will set a default IP address (192.168.1.17) in the PLC200. \
Using the Wago Ethernet setting software change the IP address to something of your liking. \
Now you can access the web configurator page 192.168.1.10/wbm through your web browser. \
Default username and password are :
    
    user        :   admin
    password    :   wago

Passworrd has been changed to :

    user        :   admin
    password    :   1234

## Linux 

### Build Linux

First update all the submodules

```
git submodule init
git submodule update
```



[FOLLOW THIS LINK](pfc-firmware-sdk-G2/README.md)

To set a static IP address you can modify the [following file](rootfs_overlay/etc/specific/netconfd.json) it will be copied to the rootfs during the deployment. \
To copy the generated OS use the deploy script
    
    ./deploy -d <sdX>

Replace "sdX" with your device

## DALI configurator

!!! Important the PLC Runtime configuration must be set on eRuntime under Configuration/PLC_Runtime !!!

The following wago modules are needed :

|NAME                               |MODEL NUMBER   |DESCRIPTION        |
|-----------------------------------|---------------|-------------------|
|PLC200                             |750-8212       |Controller         |
|DALI Multi-Master DC/DC Converter  |753-620        |DALI Power supply  |
|DALI Multi-Master                  |753-647        |DALI Master        |
|End Module                         |750-600        |                   |
|Tridonic atco                      |86455066       |Led controller     |

Build schematic:

![Build schematics][2]

Download and open [Wago DALI Configurator](https://www.wago.com/global/d/6077) and follow the following [tutorial](https://www.youtube.com/watch?v=FaoOY2-VFVk). \
!!! Disable the Only Unadressed Devices checkbox !!!

Once you have configured all the DALI devices save your project and export it to e!Cockpit. \
Open e!Cockpit and create an empty project. Under Network use the Scan button to find your device. On

### Change serial interface owner

You can connect to the linux console either though ssh or the serial port. To use the serial port you must give the control of this port to Linux. This can be done through the web interface under Configuration/Administration/Serial-Interface a assign the serial console to Linux. 

![Web management interface][1]

The default username and password are for linux :

    user        : root
    password    : wago

### Software deployment


[1]: <doc/img/wbm-serial.png>
[2]: <doc/img/WAGO_Schematics.drawio.png>